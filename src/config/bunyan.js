const pjson = require('../../package.json');

module.exports = {
    name: pjson.name,
    streams: [
        {
            level: 'debug',
            stream: process.stdout,
        },
        {
            level: 'error',
            path: 'logs/error.log',
        },
        {
            level: 'info',
            path: 'logs/info.log',
        },
    ],
};
