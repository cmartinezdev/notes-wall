module.exports = {
    uri: process.env.MONGO_URI || 'mongodb://localhost:27017/dev_notes_wall',
    options: {
        native_parser: true,
        //uri_decode_auth: true,
    },
    collections: {
        user: 'user',
        note: 'note',
    },
};
