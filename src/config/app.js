module.exports = {
    port: process.env.PORT || 3003,
    env: process.env.ENV || 'development',
};
