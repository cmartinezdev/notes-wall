const fs = require('fs');

module.exports = {
    format: 'combined',
    options: {
        stream: fs.createWriteStream('logs/access.log', {flags: 'a'}),
    },
};
