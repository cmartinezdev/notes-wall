const Q = require('q');
const jwt = require('jsonwebtoken');

/**
 * Class for jwt auth
 */
class JWTAuth {

    /**
     *
    */
    /**
     * Options for Note search query.
     * @typedef {Object} JwtAuthConstructorData
     * @property {String} [header] User id
     * @property {String} [secret] - Min create date
     * @property {Object} [options] - JWT configuration options {@link https://github.com/auth0/node-jsonwebtoken}
     */

    /**
     * Constructor of the class
     * @param {JwtAuthConstructorData} jwtAuthConstructorData Object with all JWTAuth module configuration
     *
     * @returns {QPromise<Note[]>} Return a promise with a list of notes
     */
    constructor({header, secret, options} = {}) {
        this.header = header || 'token';
        this.secret = secret ||  'secret';
        this.options = Object.assign({
            expiresIn: '1h'
        }, options || {});
    }

    /**
     * Creates and sign a token
     * @param {Object} payload  Content of the token
     *
     * @returns {Promise<String>} Promise with a signed token.
     */
    sign(payload) {
        let deferred = Q.defer();

        jwt.sign(payload, this.secret, this.options, (err, token) => {
            if(err) {
                return deferred.reject(err);
            }

            deferred.resolve(token);
        });

        return deferred.promise;
    }

    /**
     * Verifies and decode a token
     * @param {String} token Token to verify
     *
     * @returns {object} Payload of the token
     */
    verify(token) {
        let deferred = Q.defer();

        jwt.verify(token, this.secret, this.options, (err, token) => {
            if(err) {
                return deferred.reject(err);
            }

            deferred.resolve(token);
        });

        return deferred.promise;
    }

    /**
     * Method to configure headers to expose
     * @returns {Function} Middleware to configre expose headers
     */
    configureExposeHeader() {
        return (req, res, next) => {
            res.set('Access-Control-Expose-Headers', this.header);

            next();
        };
    }

    /**
     * Set the token header
     * @param {Object} res Express response object
     *
     * @returns {Function} Function that accepts an object as argument. This object must have the payload property. That funcion returns a Promise that resolves with the object received as argument.
     */
    setToken(res) {
        return obj => this.sign(obj.payload).
            then(token => {
                res.set(this.header, token);
                return obj;
            });
    }

    /**
     * Function that return a Express middleware to verify auth
     *
     * @returns {Function} Express middleware
     */
    ensureAuthorized() {
        return (req, res, next) => {
            if(!res.locals.token) {
                return next({
                    status: 403,
                    message: 'Unauthorized',
                });
            }

            next();
        };
    }

    /**
     * Function that returns an express middleware for handling tokens
     * @returns {Function} Express middleware
     */
    handleToken() {
        return (req, res, next) => {
            let token = req.get(this.header);

            if(!token) {
                return next();
            }

            this.verify(token).
                then(decoded => {
                    let cleaned = this.cleanDecoded(decoded);

                    res.locals.token = decoded;

                    if(!this.refresh) {
                        return next();
                    }

                    return this.sign(cleaned).then(newToken => {
                        res.set(this.header, newToken);
                    });
                }).
                catch(err => {
                    next({
                        status: 401,
                        message: err.message,
                        error: err,
                    });
                });
        };
    }

    cleanDecoded(decoded) {
        let res = Object.assign({}, decoded);

        delete res.iat;
        delete res.exp;

        return res;
    }
}

module.exports = JWTAuth;
