const config = require('../config');
const JWTAuth = require('./JWTAuth');
const mongoskin = require('mongoskin');
const ObjectID = mongoskin.ObjectID;
const logger = require('./logger');

let common = {};

(function(common) {

    common.config = config;

    common.db = mongoskin.db(config.mongodb.uri, config.mongodb.options);
    common.db.on('error', err => logger.error(err));

    common.toObjectId = function(id) {
        return typeof id == 'string' ? new ObjectID(id) : id;
    };

    common.jwt = new JWTAuth(config.jwt);
}(common));

module.exports = common;
