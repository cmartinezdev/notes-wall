const express = require('express');

let router = new express.Router();

let userRouter = require('./user');
let noteRouter = require('./note');

router.use('/user', userRouter);
router.use('/note', noteRouter);

module.exports = router;
