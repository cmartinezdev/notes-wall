const Note = require('../../../models/Note');

/**
 * Class that contains the middlewares for handling the note actions
 */
class NoteMiddlewares {
    /**
     * Empty constructor
     */
    constructor() {}

    /**
     * Middleware that retrieves a list of notes
     * @param {*} req Express request object
     * @param {*} res Express response object
     * @param {*} next Express next function
     * @returns {undefined} Nothing
     */
    get(req, res, next) {
        let {_userId, userName, created_after, created_before, page, page_size} = req.query;

        if(created_after && isNaN(created_after) || created_before && isNaN(created_before)) {
            return next({status: 400, message: '"created_after" and "created_before" must be a number.'});
        }

        let findParams = {
            userId: _userId,
            userName,
            minDate: created_after && new Date(+created_after),
            maxDate: created_before && new Date(+created_before),
            page,
            pageSize: page_size
        };

        Note.find(findParams).
            then(notes => res.json({d: {notes}})).
            catch(next);
    }

    /**
     * Middleware that creates a new note in the DB
     * @param {*} req Express request object
     * @param {*} res Express response object
     * @param {*} next Express next function
     * @returns {undefined} Nothing
     */
    post(req, res, next) {
        let {title, content} = req.body;
        let _userId = res.locals.token._id;
        let userName = res.locals.token.name;

        if(!title || !content) {
            return next({status: 400, message: '"title" and "content" are mandatory.'});
        }

        let note = new Note({title, content, _userId, userName});

        note.save().
            then(note => res.json({d: {note}})).
            catch(next);
    }

    /**
     * Middleware that modifies a note
     * @param {*} req Express request object
     * @param {*} res Express response object
     * @param {*} next Express next function
     * @returns {undefined} Nothing
     */
    put(req, res, next) {
        let {_id, title, content} = req.body;
        let _userId = res.locals.token._id;

        if(!_id || !title || !content) {
            return next({status: 400, message: '"_id", title" and "content" are mandatory.'});
        }

        Note.findById(_id).
            then(note => {
                if(note._userId.toString() != _userId.toString()) {
                    return next({status: 403, message: 'You can not modify a note owned by other user'});
                }

                return Object.assign(note, {title, content}).save();
            }).
            then(note => res.json({d: {note}})).
            catch(next);
    }

    /**
     * Middleware that deletes a note
     * @param {*} req Express request object
     * @param {*} res Express response object
     * @param {*} next Express next function
     * @returns {undefined} Nothing
     */
    delete(req, res, next) {
        let {_id} = req.query;
        let _userId = res.locals.token._id;

        if(!_id) {
            return next({status: 400, message: '"_id" is mandatory.'});
        }

        Note.findById(_id).
            then(note => {
                if(note._userId.toString() != _userId.toString()) {
                    return next({status: 403, message: 'You can not modify a note owned by other user'});
                }

                return note.delete();
            }).
            then(note => res.json({d: 'ok'})).
            catch(next);
    }
}

module.exports = NoteMiddlewares;
