const express = require('express');

const NoteMiddleware = require('./note.middlewares');
const noteMiddleware = new NoteMiddleware();

const jwt = require('../../../utils/common').jwt;

let router = new express.Router();

/* GET users listing. */
/**
 * @api {get} /user/:id Gets a list of notes
 * @apiName GetNotes
 * @apiGroup Note
 *
 * @apiParam {String} [_userId] Users unique ID.
 * @apiParam {String} [userName] User name.
 * @apiParam {Number} [created_after] Timestamp.
 * @apiParam {Number} [created_before] Timestamp.
 *
 * @apiParam {Number} [page] Page.
 * @apiParam {Number} [page_size] Page size.
 *
 * @apiHeader {String} token Authorization token.
 *
 * @apiSuccess (200) {Object} d Contains an array of notes: {"d": {"notes": []}}
 *
 * @apiSampleRequest /api/note
 */
router.get('/', jwt.handleToken(), jwt.ensureAuthorized(), noteMiddleware.get);

/**
 * @api {post} /note Creates a note
 * @apiName CreateNote
 * @apiGroup Note
 * @apiDescription Creates a note owned by an user.
 *
 * @apiParam {String} title Title of the note.
 * @apiParam {String} content Content of the note.
 *
 * @apiHeader {String} token Authorization token.
 *
 * @apiSuccess (200) {Object} d Contains note data: {"d": {"note": {"_id": "59df1e4ff7958f2b41451e16","_userId": "59d89574ddd6ef0aa9a082b7","title": "asd","content": "asd","createdAt": "2017-10-12T07: 48: 31.767Z"}}}
 *
 * @apiSampleRequest /api/note
 */
router.post('/', jwt.handleToken(), jwt.ensureAuthorized(), noteMiddleware.post);

/**
 * @api {put} /note Updates a note
 * @apiName UpdateNote
 * @apiGroup Note
 * @apiDescription Updates a note owned by an user.
 *
 * @apiParam {String} _id Id of the note.
 * @apiParam {String} title Title of the note.
 * @apiParam {String} content Content of the note.
 *
 * @apiHeader {String} token Authorization token.
 *
 * @apiSuccess (200) {Object} d {"d": {"note": {"_id": "59df1e4ff7958f2b41451e16","_userId": "59d89574ddd6ef0aa9a082b7","title": "asd","content": "asd","createdAt": "2017-10-12T07: 48: 31.767Z"}}}
 *
 * @apiSampleRequest /api/note
 */
router.put('/', jwt.handleToken(), jwt.ensureAuthorized(), noteMiddleware.put);

/**
 * @api {delete} /note Deletes a note
 * @apiName DeleteNote
 * @apiGroup Note
 * @apiDescription Deletes a note owned by an user.
 *
 * @apiParam {String} _id Id of the note.
 *
 * @apiHeader {String} token Authorization token.
 *
 * @apiSuccess (200) {Object} d Contains note data: {"d": {"deleted": "ok"}}
 *
 * @apiSampleRequest /api/note
 */
router.delete('/', jwt.handleToken(), jwt.ensureAuthorized(), noteMiddleware.delete);

module.exports = router;
