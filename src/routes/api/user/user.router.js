const express = require('express');

const UserMiddlewares = require('./user.middlewares');
const userMiddlewares = new UserMiddlewares();

let router = new express.Router();

/**
 * @api {post} /user/signup Signs up an user
 * @apiName SignUp
 * @apiGroup User
 * @apiDescription Creates an user in the database and returns user info and the auth token in the "token" header.
 *
 * @apiParam {string} name User name.
 * @apiParam {string} pass User pass.
 *
 * @apiSuccess (200) {Object} d Contains user data: {"user": {"_id": "59d89574ddd6ef0aa9a082b7", "name": "carlos"}}
 * @apiSuccess (200) {String} token Contains token
 *
 * @apiSampleRequest /api/user/signup
 */
router.post('/signup', userMiddlewares.signUp);

/**
 * @api {post} /user/signin Signs in an user
 * @apiName SignIp
 * @apiGroup User
 * @apiDescription Signs in an user and returns user info and the auth token in the "token" header.
 *
 * @apiParam {string} name User name.
 * @apiParam {string} pass User pass.
 *
 * @apiSuccess (200) {Object} d Contains user data: {"user": {"_id": "59d89574ddd6ef0aa9a082b7", "name": "carlos"}}
 * @apiSuccess (200) {String} token Contains token
 *
 * @apiSampleRequest /api/user/signin
 */
router.post('/signin', userMiddlewares.signIn);


module.exports = router;
