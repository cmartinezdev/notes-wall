const User = require('../../../models/User');
const jwt = require('../../../utils/common').jwt;

/**
 * Class that contains the middlewares for handling the user actions
 */
class UserMiddlewares {
    /**
     * Empty constructor
     */
    constructor() {}

    /**
     * Middleware that creates a new user in the DB and returns token in the response headers and user data in the response body
     * @param {*} req Express request object
     * @param {*} res Express response object
     * @param {*} next Express next function
     * @returns {undefined} Nothing
     */
    signUp(req, res, next) {
        let {name, pass} = req.body;

        if(!name || !pass) {
            return next({status: 400, message: '"name" and "pass" are mandatory.'});
        }

        User.signup(name, pass).
            then(jwt.setToken(res)).
            then(user => res.json({d: {user}, token: res.get(jwt.header)})).
            catch(next);
    }

    /**
     * Middleware that logs in user in the DB and returns token in the response headers and user data in the response body
     * @param {*} req Express request object
     * @param {*} res Express response object
     * @param {*} next Express next function
     * @returns {undefined} Nothing
     */
    signIn(req, res, next) {
        let {name, pass} = req.body;

        if(!name || !pass) {
            return next({status: 400, message: '"name" and "pass" are mandatory.'});
        }

        User.signin(name, pass).
            then(jwt.setToken(res)).
            then(user => res.json({d: {user}, token: res.get(jwt.header)})).
            catch(next);
    }
}

module.exports = UserMiddlewares;
