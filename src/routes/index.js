const express = require('express');
const router = new express.Router();

const apiRouter = require('./api');

router.use('/api', apiRouter);

module.exports = router;
