angular.module('noteswall.controllers', ['noteswall.services', 'ngStorage']).

    controller('MainController', function($scope, $state, $stateParams, $mdDialog, tokenService, notesService, $window) {
        $scope.user = tokenService.getData();

        $scope.filter = {};

        $scope.signOut = function() {
            tokenService.delete();
            $state.go('login');
        };

        $scope.openUserMenu = function($mdMenu, ev) {
            $mdMenu.open(ev);
        };

        $scope.openNoteDialog = function(ev, note = {}) {
            $scope.noteFormData = angular.merge({}, note);

            $mdDialog.show({
                scope: $scope,
                preserveScope: true,
                templateUrl: 'app/templates/note.html',
                clickOutsideToClose: true,
                fullscreen: true
            }).then(load => load && loadNotes(true), () => {});
        };

        $scope.closeNoteDialog = function() {
            $mdDialog.hide(false);
        };

        $scope.submitNoteForm = function() {
            let note = $scope.noteFormData;
            if(!note.title || !note.content) {
                return console.error('Fill title and content');
            }

            notesService[note._id ? 'edit' : 'create'](note, (err, res) => {
                if(err) {
                    console.error(err);
                    // TODO handle error
                    return;
                }

                $mdDialog.hide(true);
            });
        };

        $scope.deleteNote = function(ev, note) {
            var confirm = $mdDialog.confirm().
                title('Would you like to delete your note?').
                targetEvent(ev).
                ok('Yes, do it!').
                cancel('No');

            $mdDialog.show(confirm).then(function() {
                notesService.delete(note._id, (err, res) => {
                    if(err) {
                        console.error(err);
                        // TODO handle error
                        return;
                    }

                    loadNotes(true);
                });
                $scope.status = 'You decided to get rid of your debt.';
            }, () => {});
        };

        $scope.loadMyNotes = function() {
            $scope.filter.userName = $scope.user.name;
        };

        let _pageSize = 10;
        let _loadedPage = -1;
        let _loading = false;
        let _moreToLoad = true;
        let _numLoaded = 0;
        function loadNotes(clear) {
            if(_loading) {
                return;
            }

            _loading = true;

            if(clear) {
                $scope.notes = [];
                _loadedPage = -1;
                _moreToLoad = true;
                _numLoaded = 0;
            }

            if(!_moreToLoad) {
                return;
            }

            let params = angular.merge({page: _loadedPage + 1, page_size: _pageSize}, $scope.filter || {});
            params.created_after = params.created_after && params.created_after.getTime();
            params.created_before = params.created_before && (params.created_before.getTime() + 24 * 60 * 60 * 1000 - 1);

            notesService.get(params, (err, res) => {
                _loading = false;

                if(err) {
                    console.error(err);
                    // TODO handle error
                    return;
                }

                let newNotes = res.d.notes.
                    map(note => {
                        note.date = moment(note.createdAt).format('MMMM Do YYYY, h:mm:ss a');
                        return note;
                    });

                $scope.notes = $scope.notes.concat(newNotes);
                _loadedPage ++;
                _moreToLoad = $scope.notes.length != _numLoaded;
                _numLoaded = $scope.notes.length;
            });
        };

        $scope.loadNotes = loadNotes;

        loadNotes(true);

        angular.element($window).bind("scroll", function() {
            var windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
            var body = document.body, html = document.documentElement;
            var docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight,  html.scrollHeight, html.offsetHeight);
            var windowBottom = windowHeight + window.pageYOffset;
            if (windowBottom >= docHeight) {
                console.log("añaññ")
                loadNotes();
            }
        });
    }).

    controller('LogInController', function($scope, $state, authService) {

        $scope.signUpSubmit = function() {
            handleSubmit('signUp');
        };

        $scope.signInSubmit = function() {
            handleSubmit('signIn');
        };

        function handleSubmit(submitTipe) {
            $scope.loginForm.$setSubmitted();

            if($scope.loginForm.$invalid) {
                return;
            }

            authService[submitTipe]($scope.formdata, (err, res) => {
                if(err) {
                    console.error(err);
                    //TODO handle error
                    return;
                }

                $state.go('main');
            });
        }
    });

