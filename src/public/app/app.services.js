angular.module('noteswall.services', ['ngStorage']).
    factory('urlService', function() {
        let env = 'prod';
        var urls = {
            local: 'http://localhost:3003/api',
            prod: 'https://notes-wall.herokuapp.com/api',
        };

        var urlService = {};

        urlService.get = function() {
            return urls[env];
        };

        return urlService;
    }).

    factory('authService', function($localStorage, $http, urlService) {
        var authService = {};

        var server_url = urlService.get() + '/user';

        authService.signUp = function(formData, callback) {
            $http.post(server_url + '/signup', formData).
                then(res => callback(null, res.data), callback);
        };

        authService.signIn = function(formData, callback) {
            $http.post(server_url + '/signin', formData).
                then(res => callback(null, res.data), callback);
        };



        return authService;
    }).

    factory('notesService', function($localStorage, $http, urlService) {
        var notesService = {};

        var server_url = urlService.get() + '/note';

        notesService.get = function(params, callback) {
            $http.get(server_url, {params}).
                then(res => callback(null, res.data), callback);
        };

        notesService.create = function(params, callback) {
            $http.post(server_url, params).
                then(res => callback(null, res.data), callback);
        };

        notesService.edit = function(params, callback) {
            $http.put(server_url, params).
                then(res => callback(null, res.data), callback);
        };

        notesService.delete = function(_id, callback) {
            $http.delete(server_url, {params: {_id}}).
                then(res => callback(null, res.data), callback);
        };

        return notesService;
    }).

    factory('tokenService', function($localStorage) {
        var tokenService = {};
        var tokenData = {};

        loadTokenData();

        tokenService.exists = function() {
            return Boolean($localStorage.token);
        };

        tokenService.get = function(token) {
            return $localStorage.token;
        };

        tokenService.set = function(token) {
            $localStorage.token = token;
            loadTokenData();
        };

        tokenService.delete = function() {
            delete $localStorage.token;
        };

        tokenService.getData = function() {
            return tokenData;
        };

        function loadTokenData() {
            tokenData = $localStorage.token ? JSON.parse(atob($localStorage.token.split('.')[1])) : {};
        }

        return tokenService;
    });
