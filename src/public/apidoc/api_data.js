define({ "api": [
  {
    "type": "post",
    "url": "/note",
    "title": "Creates a note",
    "name": "CreateNote",
    "group": "Note",
    "description": "<p>Creates a note owned by an user.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Title of the note.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>Content of the note.</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Authorization token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "d",
            "description": "<p>Contains note data: {&quot;d&quot;: {&quot;note&quot;: {&quot;_id&quot;: &quot;59df1e4ff7958f2b41451e16&quot;,&quot;_userId&quot;: &quot;59d89574ddd6ef0aa9a082b7&quot;,&quot;title&quot;: &quot;asd&quot;,&quot;content&quot;: &quot;asd&quot;,&quot;createdAt&quot;: &quot;2017-10-12T07: 48: 31.767Z&quot;}}}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/api/note"
      }
    ],
    "version": "0.0.0",
    "filename": "src/routes/api/note/note.router.js",
    "groupTitle": "Note"
  },
  {
    "type": "delete",
    "url": "/note",
    "title": "Deletes a note",
    "name": "DeleteNote",
    "group": "Note",
    "description": "<p>Deletes a note owned by an user.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Id of the note.</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Authorization token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "d",
            "description": "<p>Contains note data: {&quot;d&quot;: {&quot;deleted&quot;: &quot;ok&quot;}}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/api/note"
      }
    ],
    "version": "0.0.0",
    "filename": "src/routes/api/note/note.router.js",
    "groupTitle": "Note"
  },
  {
    "type": "get",
    "url": "/user/:id",
    "title": "Gets a list of notes",
    "name": "GetNotes",
    "group": "Note",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "_userId",
            "description": "<p>Users unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "userName",
            "description": "<p>User name.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "created_after",
            "description": "<p>Timestamp.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "created_before",
            "description": "<p>Timestamp.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "description": "<p>Page.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page_size",
            "description": "<p>Page size.</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Authorization token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "d",
            "description": "<p>Contains an array of notes: {&quot;d&quot;: {&quot;notes&quot;: []}}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/api/note"
      }
    ],
    "version": "0.0.0",
    "filename": "src/routes/api/note/note.router.js",
    "groupTitle": "Note"
  },
  {
    "type": "put",
    "url": "/note",
    "title": "Updates a note",
    "name": "UpdateNote",
    "group": "Note",
    "description": "<p>Updates a note owned by an user.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Id of the note.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Title of the note.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>Content of the note.</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Authorization token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "d",
            "description": "<p>{&quot;d&quot;: {&quot;note&quot;: {&quot;_id&quot;: &quot;59df1e4ff7958f2b41451e16&quot;,&quot;_userId&quot;: &quot;59d89574ddd6ef0aa9a082b7&quot;,&quot;title&quot;: &quot;asd&quot;,&quot;content&quot;: &quot;asd&quot;,&quot;createdAt&quot;: &quot;2017-10-12T07: 48: 31.767Z&quot;}}}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/api/note"
      }
    ],
    "version": "0.0.0",
    "filename": "src/routes/api/note/note.router.js",
    "groupTitle": "Note"
  },
  {
    "type": "post",
    "url": "/user/signin",
    "title": "Signs in an user",
    "name": "SignIp",
    "group": "User",
    "description": "<p>Signs in an user and returns user info and the auth token in the &quot;token&quot; header.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>User name.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pass",
            "description": "<p>User pass.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "d",
            "description": "<p>Contains user data: {&quot;user&quot;: {&quot;_id&quot;: &quot;59d89574ddd6ef0aa9a082b7&quot;, &quot;name&quot;: &quot;carlos&quot;}}</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Contains token</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/api/user/signin"
      }
    ],
    "version": "0.0.0",
    "filename": "src/routes/api/user/user.router.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/user/signup",
    "title": "Signs up an user",
    "name": "SignUp",
    "group": "User",
    "description": "<p>Creates an user in the database and returns user info and the auth token in the &quot;token&quot; header.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>User name.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pass",
            "description": "<p>User pass.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "d",
            "description": "<p>Contains user data: {&quot;user&quot;: {&quot;_id&quot;: &quot;59d89574ddd6ef0aa9a082b7&quot;, &quot;name&quot;: &quot;carlos&quot;}}</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Contains token</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/api/user/signup"
      }
    ],
    "version": "0.0.0",
    "filename": "src/routes/api/user/user.router.js",
    "groupTitle": "User"
  }
] });
