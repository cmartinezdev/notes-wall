const common = require('../utils/common');
const Q = require('q');
const bcrypt = require('bcrypt-nodejs');

/**
 * Represents an user
 */
class User {
    /**
     * Constructor for User class
     * @constructor
     * @param {ObjectId} _id MongoDB ObjectId
     * @param {String} name User name
     */
    constructor(_id, name) {
        this._id = common.toObjectId(_id);
        this.name = name;
    }

    /**
     * User mongodb collection
     */
    static get collection() {
        return common.db.collection(common.config.mongodb.collections.user);
    }

    /**
     * Signs up an user and returns a promise with the user
     * @param {String} name User name
     * @param {String} pass User pass
     *
     * @returns {QPromise<User>} Promise with the result
     */
    static signup(name, pass) {
        if(!name || !pass) {
            return Q.reject({message: '"name" and "pass" are mandatory.'});
        }

        let deferred = Q.defer();

        this.hash = bcrypt.hash(pass, null, null, (err, hash) => {
            if(err) {
                return deferred.reject(err);
            }

            this.collection.findOne({name}, (err, user) => {
                if(err) {
                    return deferred.reject(err);
                }

                if(user) {
                    return deferred.reject({message: 'User "name" is in use.'});
                }

                this.collection.insertOne({name, hash}, (err, result) => {
                    if(err) {
                        return deferred.reject(err);
                    }

                    this._id = result.insertedId;

                    deferred.resolve(new User(result.insertedId, name));
                });
            });
        });

        return deferred.promise;
    }

    /**
     * Signs in an user and returns a promise with the user
     * @param {String} name User name
     * @param {String} pass User pass
     *
     * @returns {QPromise<User>} Promise with the result
     */
    static signin(name, pass) {
        if(!name || !pass) {
            return Q.reject({message: '"name" and "pass" are mandatory.'});
        }

        let deferred = Q.defer();

        this.collection.findOne({name}, (err, user) => {
            if(err) {
                return deferred.reject(err);
            }

            if(!user) {
                return deferred.reject({message: 'User does not exist.'});
            }

            bcrypt.compare(pass, user.hash, (err, ok) => {
                if(err) {
                    return deferred.reject(err);
                }

                if(!ok) {
                    return deferred.reject('Invalid password.');
                }

                return deferred.resolve(new User(user._id, name));
            });
        });

        return deferred.promise;
    }

    /**
     * Return a payload to sign the token
     *
     * @returns {Object} Payload to sign the token
     */
    get payload() {
        return {
            _id: this._id,
            name: this.name,
        };
    }
}

module.exports = User;
