const common = require('../utils/common');
const logger = require('../utils/logger');
const Q = require('q');

/**
 * Class that represents a note
 */
class Note {
    /**
     * Options for Note contructor.
     * @typedef {Object} NoteConstructorData
     * @property {String | ObjectId} [_id] Note id
     * @property {String | ObjectId} _userId User id
     * @property {String} userName User name
     * @property {String} title - Note title
     * @property {String} content - Note content
     * @property {Date} [createdAt] - Note creation date
     */
    /**
     * Constructor for Note
     * @param {NoteConstructorData} noteConstructorData Object containing all note data.
     */
    constructor({_id, _userId, userName, title, content, createdAt} = {}) {
        this._id = common.toObjectId(_id);
        this._userId = common.toObjectId(_userId);
        this.userName = userName;
        this.title = title;
        this.content = content;
        this.createdAt = createdAt || new Date();
    }

    /**
     * Mongodb collection for notes
     */
    static get collection() {
        return common.db.collection(common.config.mongodb.collections.note);
    }

    /**
     * Mongodb collection for notes
     */
    get collection() {
        return common.db.collection(common.config.mongodb.collections.note);
    }

    /**
     * Finds a note and return a Promise with it
     * @param {String} id Note identifier
     *
     * @returns {QPromise<Note>} Promise with the note, or indefined if note with that id doesn't exist
     */
    static findById(id) {
        if(!id) {
            return Q.reject({message: '"id" is missed.'});
        }

        let _id = common.toObjectId(id);

        let deferred = Q.defer();

        this.collection.findOne({_id}, (err, noteData) => {
            if(err) {
                return deferred.reject(err);
            }

            deferred.resolve(noteData ? new Note(noteData) : undefined);
        });

        return deferred.promise;
    }

    /**
     * Options for Note search query.
     * @typedef {Object} NoteQueryData
     * @property {String | ObjectId} [_userId] User id
     * @property {String} [userName] User name
     * @property {Date} [minDate] - Min create date
     * @property {Date} [maxDate] - Max create date
     * @property {Number} [page] - Page number
     * @property {Number} [pageSize] - Page size
     */

    /**
     * Finds a list of notes
     * @param {NoteQueryData} noteQueryData Object with all the search data
     *
     * @returns {QPromise<Note[]>} Return a promise with a list of notes
     */
    static find({userId, userName, minDate, maxDate, page, pageSize} = {}) {
        let query = {};

        if(userId) {
            query._userId = common.toObjectId(userId);
        }
        if(userName) {
            query.userName = userName;
        }
        if(minDate || maxDate) {
            query.createdAt = {};
        }
        if(minDate) {
            query.createdAt.$gte = minDate;
        }
        if(maxDate) {
            query.createdAt.$lte = maxDate;
        }

        logger.debug('Search query', query);

        let deferred = Q.defer();

        this.collection.find(query).
            sort({'createdAt': -1}).
            limit(+pageSize || 10).
            skip(page * pageSize).
            toArray((err, notesData) => {
                if(err) {
                    return deferred.reject(err);
                }

                deferred.resolve(notesData.map(noteData => new Note(noteData)));
            });

        return deferred.promise;
    }

    /**
     * Stores a note in the DB
     *
     * @returns {Promise<Note>} Promise with the modified note
     */
    save() {
        let deferred = Q.defer();

        if(this._id) {
            this.collection.updateOne({_id: this._id}, {$set: {title: this.title, content: this.content}}, (err, result) => {
                if(err) {
                    return deferred.reject(err);
                }

                if(!result.result.ok) {
                    return deferred.reject({message: 'Error updating note'});
                }

                deferred.resolve(this);
            });
        } else {
            this.collection.insertOne({title: this.title, content: this.content, createdAt: this.createdAt, _userId: this._userId, userName: this.userName}, (err, result) => {
                if(err) {
                    return deferred.reject(err);
                }

                this._id = result.insertedId;

                deferred.resolve(this);
            });
        }

        return deferred.promise;
    }

    /**
     * Deletes a note
     *
     * @returns {Promise} Returns an empty promise when finished
     */
    delete() {
        if(!this._id) {
            return Q.reject({message: '"id" is missed'});
        }

        let deferred = Q.defer();

        this.collection.deleteOne({_id: this._id}, (err, result) => {
            if(err) {
                return deferred.reject(err);
            }

            if(!result.result.ok) {
                return deferred.reject({message: 'Error. Can not delete that note.'});
            }

            deferred.resolve();
        });

        return deferred.promise;
    }
}

module.exports = Note;
