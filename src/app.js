const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');

const logger = require('./utils/logger');
const common = require('./utils/common');

const config = require('./config');

const routes = require('./routes');

const app = express();

app.set('env', config.app.env);

app.use(cors());
app.use(common.jwt.configureExposeHeader());
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(morgan(config.morgan.format, config.morgan.options));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    logger.error(err);
    let message = err.message || 'error';
    let error = req.app.get('env') === 'development' ?
        JSON.parse(JSON.stringify(err, Object.getOwnPropertyNames(err))) :
        {};

    res.
        status(err.status || 500).
        json({message, error});
});

module.exports = app;
